#!/usr/bin/env perl

use strict;
use warnings;

use File::HomeDir;

my $usage = "Usage: $0 <user> <repository>\n";

my $user = shift;
my $repo = shift;
print($usage), exit unless $user && $repo;

my $github = File::HomeDir->my_home . "/src/github/";
die "$! $github" unless -d $github;

my $remote = "git://github.com/";

system("git", "clone",
       $remote . $user . "/" . $repo . ".git",
       $github . $user . "/" . $repo);
