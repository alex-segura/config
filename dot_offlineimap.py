import re, os

def get_password_emacs(pass_entry):
    s = os.popen("pass %s" % pass_entry).read().strip()
    if s == '':
        return None
    return s.split('\n')[0]
