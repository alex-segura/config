#!/usr/bin/env perl
use strict;
use warnings;

use Getopt::Std;
use Path::Tiny;

my %opts = (
        'f' => 0,
        'i' => '.',
        'o' => '~',
);

getopts "fi:o:", \%opts;

my $force = $opts{'f'};
my $in = $opts{'i'};
my $out = $opts{'o'};

die "$! $in" unless path($in)->is_dir;
die "$! $out" unless path($out)->is_dir;

for my $file (grep /dot_/, path($in)->children) {
        my $from = path $file;
        $file =~ s/dot_/\./g;
        my $to = path $out, path($file)->basename;

        $to->remove if (-l $to && $force);

        if (-l $to) {
                printf "Link exists: %s\n", $to;
                next;
        }

        printf "%s -> %s\n", $from, $to;
        my $ok = symlink($from->stringify, $to->stringify);
        print "$!\n" if !$ok;
}
