#!/bin/ksh

PS1="\\$ "
export PS1

if [ -z "$VISUAL" -a -z "$EDITOR" ]; then
    set -o emacs
fi
